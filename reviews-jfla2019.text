> ----------------------- REVIEW 1 ---------------------
> PAPER: 8
> TITLE: Unboxing Mutually Recursive Type Definitions
> AUTHORS: Rodolphe Lepigre, Gabriel Scherer and Simon Colin
>
> Overall evaluation: 3 (strong accept)
>
> ----------- Overall evaluation -----------
> Le compilateur OCaml implémente une optimisation facultative pour éliminer
> une indirection dans les représentations de types qui n'ont qu'un seul
> constructeur. Une attention spéciale doit être prêtée à l'interaction de
> cette optimisation avec une autre qui vise à éliminer une indirection dans
> la représentation de flottants. Un contrôle spécifique est implémenté dans
> le compilateur pour rejeter des déclarations susceptibles à engendrer de
> mauvaises interactions et permettre des erreurs de segmentation.
>
> Cet article présent la première formalisation du ledit contrôle. Cette
> formalisation a permis les auteurs à généraliser les règles pour accepter
> plus de programmes, à simplifier son implémentation dans le compilateur
> OCaml et à esquisser des preuves de sa correction et de sa complétude.
>
> L'interaction des deux optimisations étonnamment subtile, notamment pour
> traiter les GADTs, et mérite le traitement formel proposé dans l'article.
> Les auteurs se servent de la formalisation pour donner des explications
> lucides et précises sur le problème et la solution qu'ils proposent. Ils
> expliquent bien les intuitions derrière leur travail et mettre en opposition
> les règles déclaratives et les détails d'une vraie implémentation. L'annexe
> présente une sémantique qui permet d'énoncer les lemmes de correction et de
> complétude et d'esquisser leurs preuves.
>
> J'ai beaucoup apprécié cet article et je trouve qu'il est très pertinent
> pour les JFLAs.
>
> Je n'ai que deux petites questions et quelques remarques mineures sur la
> forme.
>
> Q1. Faut-il exposer les types de séparabilité dans les interfaces OCaml,
> c.-à-d., les fichiers *.mli ?
>
> Q2. Faut-il un traitement spécial pour les références ?

J'ai clarifié dans l'article que seul les enregistrements avec un seul
champ *non modifiable* (immutable) peuvent être unboxés.

Il serait pertinent de dire que le type `'a ref`, défini comme un enregistrement

    type 'a ref = { mutable contents : 'a }

est (forcément) boxé et donc a la signature `('a : Ind) ref`, mais je
n'ai pas trouvé le bon endroit dans l'article pour le
dire. (On pourrait imaginer une sous-section, dans "Section 5:
Integration to OCaml", qui parle de tous les types OCaml dont on n'a
pas parlé : référence, variants polymorphes, objets etc.)

> Suggestions mineures et petites corrections
> ----------------------------------------
>
> * Est-ce que le "G" dans GADT veut dire "generalized" ou "guarded"?

J'ai ajouté une footnote.

> * Il y a beaucoup de petites remarques en parenthèses partout dans le papier.
>   Certaines sont utiles, mais d'autres ne sont que des distractions. Je vous
>   suggère d'essayer, pour chaque cas, d'enlever la remarque, de supprimer
>   les parenthèses, ou de simplement utiliser des paires de virgules.
>   Quelques suggestions précises dans ce sens sont données dans mes
>   annotations détaillées.

DONE

> p.3, "expended" -> "expanded"
> §2.1 "| Char : char -> char tag" -> "| Char : char -> char data"
> §2.1 "| Bool : bool -> bool tag" -> "| Bool : bool -> bool data"
> §4: "sweet-sour": que veut-il dire ici, cet adjectif ?
> §5.2: "specific specialized" -> "specific"
> §5.2: "vec{\alpha}" -> "\vec{\alpha}"
> §5.3: "tau_1 -> "\tau_1"
> §5.4: "We mentioned in Semantics (Section A.2) that our judgements are
> non-principal - Fact 2." -> "We can show that our judgements are
> non-principal (Section A.2, Fact 2)."
> §5.4: "in Future Work 6.1" -> "in the following section"
> §6.1: "sensibly" -> "significantly"
> §6.2: "unsoud" -> "unsound"
> §6.3: "for more details)" -> ""
>
> * Sinon, j'ai annoté une copie du papier avec quelques petites suggestions.
>   Ce n'est pas possible de la télécharger dans EasyChair, je vais donc le
>   faire passer par un autre moyen.
>
>
> ----------------------- REVIEW 2 ---------------------
> PAPER: 8
> TITLE: Unboxing Mutually Recursive Type Definitions
> AUTHORS: Rodolphe Lepigre, Gabriel Scherer and Simon Colin
>
> Overall evaluation: 3 (strong accept)
>
> ----------- Overall evaluation -----------
>
> Le papier présente une variation du système de type d'OCaml permettant
> de lever certaines limitation du language.  Le but étant d'étendre les
> cas où l'on peut "unboxer" les constructeurs, en utilisant directement
> la valeur de leur unique champ comme leur propre valeur. Ceci permet
> de réduire les appels au GC d'OCaml (même de les supprimer
> complètement si le champ est une valeur simple (entier, caractère,
> bool, constructeur constant).
>
> La difficulté principale pour faire cela vient du fait de la double
> représentation des floatants dans OCaml, soit un pointeur sur une
> paire , soit directement par le floatant lui même ce qui permet un
> gain d'efficacité.  Je suis d'ailleurs surpris que cette optimisation
> sur les floatants n'ai pas été étendu au entier 32 ou 64 bits.
>
> Le papier présente une variation du système actuel permettant de
> vérifier que l'unboxing est valide en présence de floatant. Le test
> actuel est principalement syntaxique ce qui présente généralement de
> nombreux inconvénients comme le montre la condition de garde de Coq.
> La nouvelle proposition est basé sur un système de type, avec sous typage.
>
> Après une très bonne introduction du problème dans la section 1, le
> papier présente le système de type dans la section 2 et 3. Le système
> de type permet aussi de gérer les GADT grace à l'utilisation de
> quantificateurs et d'égalité sur les types. Je trouve que ceci
> complique beaucoup la présentation, peut être qu'une présentation en
> deux temps simplifierait la lecture (i.e. sans GADT puis introduction
> des GADT). La section 4 est juste une référence à l'appendice et
> peut-être supprimée. La section 5 présente l'implémentation.

[Gabriel] je ne compte pas re-découper la présentation, on perd trop
de place et c'est un changement qui demande trop de travail. On est
déjà limité en taille par la conférence, on a dû couper la section sur
la sémantique.

> Globalement, je trouve le papier bien présenté. Il propose une
> solution théorique élégante à un problème pratique (comment avoir des
> programmes plus efficaces) et fourni une implémentation qui semble
> complète. Cela me semble un papier parfait pour les JFLA.
>
>

> Même si certaines phrases du papier m'ont bien fait rire, je pense
> qu'elles peuvent être temporisées ou équilibrées (peut-être):
>
> - "those that care about software engineering with those that are
>    obsessed with performances"
>
>    La phrase donne clairement une préférence, mais peut-être est-ce
>    seulement que ma préférence personnel va vers les performances.

[Gabriel] J'ai reformulé en:

As a side-product, unboxing resolves one case of the general tension
between software engineering and performance.

>
> - "the notion of separability evidently existed in Damien Doligez's
>    head ...."
>
>    Personnellement je trouve que la phrase sonne comme un reproche
>    personnel, au moins une incompréhension par rapport au manque de
>    publication de la personne cité, je ne suis pas sur de l'apport de
>    la phrase pour le papier lui même.

[Gabriel] j'ai remplacé "Damien Doligez's head" par "Damien Doligez's
mind". On ne peut pas vraiment enlever la phrase car cela donnerait
l'impression que nous sommes les inventeurs de l'idée, alors qu'elle
est clairement présente, implicitement, dans l'implémentation de
Damien.

> Typos:
> page 6: "we need to precise define"
> page 10 : \Sigma_{blocK}   K minuscule
> page 12 : vec -> \vec
>                 tau -> \tau
> page 14 : curent implementer -> curent implementation
