#!/bin/bash
mkdir arxiv || { echo "error, you need to (rmdir arxiv)"; exit 1; }

cp unboxing.tex arxiv/
cp examples.tex arxiv/

for f in biblio.bib mathpartir.sty easychair.cls
do
    cp $f arxiv/
done

# Pre-generate the bibliography file .bbl

pdflatex -shell-escape unboxing.tex || exit 3
bibtex unboxing.aux || exit 3
cp unboxing.bbl arxiv/

# Pre-generate the cache files for 'minted'
# building the paper with finealizecache=true
# (in a temporary arxiv-prebuild directory)
# creates a version of the cache that can be used
# without depending on -shell-escape, python and pygment
# if frozencache=true is set in the final arxiv version

cp -r arxiv arxiv-prebuild
echo '\newcommand{\MINTEDOPTIONS}{finalizecache=true}' > arxiv-prebuild/minted.cfg
echo '\newcommand{\MINTEDOPTIONS}{frozencache=true}' > arxiv/minted.cfg

cp unboxing.aux arxiv-prebuild/ # silence build warnings
cd arxiv-prebuild
pdflatex -shell-escape unboxing.tex
cd ..
cp -r arxiv-prebuild/_minted-unboxing arxiv/
rm -fR arxiv-prebuild

touch arxiv/build.sh
for i in 1 2 3
do
    echo "pdflatex unboxing.tex" >> arxiv/build.sh
done

zip -r arxiv arxiv
